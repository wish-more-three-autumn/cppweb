package app;

import webx.WebApp;
import stdx.Required;
import stdx.Optional;
import webx.utils.DBConnect;
import webx.http.HttpRequest;
import webx.http.HttpResponse;

class Request{
	@Required("用户ID")
	public String user;

	@Optional("用户姓名")
	public String name;
}

class Response{
	@Required("错误码")
	public int code;

	@Optional("错误描述")
	public String desc;
}

@WebApp.Path(value = "${filename}", access = "public")
@WebApp.Document(request = Request.class, response = Response.class, remark = "JAVA示例接口")
public class JavaTest extends WebApp{
	public void process(HttpRequest request, HttpResponse response) throws Exception{
		Long msg = DBConnect.Select(Long.class, "SELECT ?", System.currentTimeMillis());

		response.setBody(String.valueOf(msg));
	}
}
