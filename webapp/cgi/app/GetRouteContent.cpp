#include <webx/menu.h>
#include <webx/route.h>
#include <dbentity/T_XG_ROUTE.h>

class GetRouteContent : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetRouteContent)

int GetRouteContent::process()
{
	param_name_string(id);

	webx::CheckSystemRight(this);

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect(dbid);

	stdx::format(sqlcmd, "SELECT CONTENT FROM T_XG_ROUTE WHERE ID='%s'", id.c_str());
	
	sp<QueryResult> rs = dbconn->query(sqlcmd);

	if (!rs) return simpleResponse(XG_SYSERR);

	sp<RowData> row = rs->next();

	if (!row) return simpleResponse(XG_NOTFOUND);

	out << row->getString(0);

	return XG_OK;
}