#include "T_XG_FILE.h"


void CT_XG_FILE::clear()
{
	this->id.clear();
	this->rid.clear();
	this->type.clear();
	this->imme.clear();
	this->remark.clear();
	this->content.clear();
	this->statetime.clear();
}
int CT_XG_FILE::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_FILE(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->rid.toValueString(conn->getSystemName());
	vec.push_back(&this->rid);
	sql += ",";
	sql += this->type.toValueString(conn->getSystemName());
	vec.push_back(&this->type);
	sql += ",";
	sql += this->imme.toValueString(conn->getSystemName());
	vec.push_back(&this->imme);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->content.toValueString(conn->getSystemName());
	vec.push_back(&this->content);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_FILE::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->rid = row->getString(1);
	this->rid.setNullFlag(row->isNull());
	this->type = row->getString(2);
	this->type.setNullFlag(row->isNull());
	this->imme = row->getString(3);
	this->imme.setNullFlag(row->isNull());
	this->remark = row->getString(4);
	this->remark.setNullFlag(row->isNull());
	this->content = row->getBinary(5);
	this->content.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(6);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_FILE::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_FILE";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_FILE::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_FILE::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_FILE::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_FILE SET ";
	if (nullable || !this->rid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "RID=";
		sql += this->rid.toValueString(conn->getSystemName());
		v.push_back(&this->rid);
	}
	if (nullable || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
		v.push_back(&this->type);
	}
	if (nullable || !this->imme.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "IMME=";
		sql += this->imme.toValueString(conn->getSystemName());
		v.push_back(&this->imme);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_FILE::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_FILE";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_FILE::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_FILE::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "RID") return this->rid.toString();
	if (key == "TYPE") return this->type.toString();
	if (key == "IMME") return this->imme.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "CONTENT") return this->content.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_FILE::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "RID")
	{
		this->rid = val;
		return true;
	}
	if (key == "TYPE")
	{
		this->type = val;
		return true;
	}
	if (key == "IMME")
	{
		this->imme = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "CONTENT")
	{
		this->content = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_FILE::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_FILE SET ";
	if (nullable || !this->rid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "RID=";
		sql += this->rid.toValueString(conn->getSystemName());
		v.push_back(&this->rid);
	}
	if (nullable || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
		v.push_back(&this->type);
	}
	if (nullable || !this->imme.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "IMME=";
		sql += this->imme.toValueString(conn->getSystemName());
		v.push_back(&this->imme);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->content.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CONTENT=";
		sql += this->content.toValueString(conn->getSystemName());
		v.push_back(&this->content);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
