#ifndef XG_HTTPHELPER_H
#define XG_HTTPHELPER_H
//////////////////////////////////////////////////////////
#include <http/HttpResponse.h>

class HttpHelper
{
public:
	static SmartBuffer GetResult(const string& url, const string& cookie = "", bool tiny = false)
	{
		return GetResultEx(url, stdx::EmptyString(), stdx::EmptyString(), cookie, tiny);
	}
	static SmartBuffer GetResult(const string& url, const HttpDataNode& data, const string& cookie = "", bool tiny = false)
	{
		return GetResultEx(url, data.toString(), stdx::EmptyString(), cookie, tiny);
	}

public:
	static int GetLastStatus();
	static void SetLastStatus(int code);
	static sp<Socket> Connect(const string& ip, int port, bool crypted);
	static int GetLinkSet(set<string>& uset, const string& html, const string& host = "");
	static string GetLink(const string& path, const string& host, int port = 80, bool crypted = false);
	static SmartBuffer GetResultEx(const string& url, const string& data, const string& contype = "", const string& cookie = "", bool tiny = false);
};
//////////////////////////////////////////////////////////
#endif