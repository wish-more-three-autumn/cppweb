#ifndef XG_LINUX

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif

#include <WINSOCK2.h>

#endif

#include "../socksvr.h"

#define CONNECTION_ACTIVE			0
#define CONNECTION_OCCUPY			1

#ifndef XG_POLL_WAIT_TIMEOUT
#define XG_POLL_WAIT_TIMEOUT		100
#endif

#ifndef XG_SOCKSVR_MAXCONNSIZE
#define XG_SOCKSVR_MAXCONNSIZE		10000
#endif

/////////////////////////////////////////////////////////////////////

typedef struct
{
#ifndef XG_LINUX
	WSAOVERLAPPED overlap;
	WSABUF buffer;
#endif
	int flag;
	time_t tm;
	SOCKET sock;
	stConnectData data;
} stConnectInfo;

static int timeout;
static HANDLE handle;
static MutexHandle mutex;
static stConnectInfo* connpool;

static void(*Lock)() = NULL;
static void(*Unlock)() = NULL;
static int(*ProcessRequest)(SOCKET, stConnectData*) = NULL;
static int(*ProcessConnectClosed)(SOCKET, stConnectData*) = NULL;
static int(*ProcessConnect)(SOCKET, stConnectData*, const char*, int) = NULL;

/////////////////////////////////////////////////////////////////////

static int PostRecv(stConnectInfo* conn);

static void MutexLock()
{
	LockMutex(mutex);
}
static void MutexUnlock()
{
	UnlockMutex(mutex);
}
void ServerSocketSetLockFunction(void(*lock)(), void(*unlock)())
{
	Lock = lock;
	Unlock = unlock;
}
void ServerSocketSetProcessFunction(int(*func)(SOCKET, stConnectData*))
{
	ProcessRequest = func;
}
void ServerSocketSetConnectClosedFunction(int(*func)(SOCKET, stConnectData*))
{
	ProcessConnectClosed = func;
}
void ServerSocketSetConnectFunction(int(*func)(SOCKET, stConnectData*, const char*, int))
{
	ProcessConnect = func;
}

/////////////////////////////////////////////////////////////////////

static void CloseConnect(stConnectInfo* conn)
{
	if (ProcessConnectClosed)
	{
		int res = ProcessConnectClosed(conn->sock, &conn->data);

		if (res == XG_DETACHCONN) conn->sock = INVALID_SOCKET;
	}

	if (conn->sock != INVALID_SOCKET)
	{
		SocketClose(conn->sock);
		conn->sock = INVALID_SOCKET;
	}
}
static stConnectInfo* MallocConnectNode(SOCKET sock, const char* address)
{
	static int len = 0;
	static time_t last = 0;
	static stConnectInfo* stack[XG_SOCKSVR_MAXCONNSIZE];

	time_t now = time(NULL);
	stConnectInfo* str = connpool;
	stConnectInfo* end = connpool + XG_SOCKSVR_MAXCONNSIZE;

	if (len == 0 || last + timeout < now)
	{
		last = now;
		len = 0;

		while (str < end)
		{
			if (str->sock == INVALID_SOCKET)
			{
				stack[len++] = str;
			}
			else
			{
				if (str->flag == CONNECTION_ACTIVE && str->tm + timeout < now)
				{
#ifdef XG_LINUX
					epoll_ctl(handle, EPOLL_CTL_DEL, str->sock, NULL);
#endif
					CloseConnect(str);
					stack[len++] = str;
				}
			}

			++str;
		}

		if (len == 0) return NULL;
	}

	str = stack[--len];
	strncpy(str->data.addr, address, sizeof(str->data.addr) - 1);
	str->data.addr[sizeof(str->data.addr) - 1] = 0;
	str->sock = sock;

	return str;
}
static void SocketServerInit(int second)
{
	int i = 0;

	SocketSetup();

	timeout = second;

	if (Lock == NULL || Unlock == NULL)
	{
		InitMutex(mutex);

		Lock = MutexLock;
		Unlock = MutexUnlock;
	}

	if ((connpool = (stConnectInfo*)malloc(sizeof(stConnectInfo) * XG_SOCKSVR_MAXCONNSIZE)) == NULL) ErrorExit(XG_SYSERR);

	while (i < XG_SOCKSVR_MAXCONNSIZE) connpool[i++].sock = INVALID_SOCKET;
}
static void ProcessCommand(stConnectInfo* conn)
{
	int res = 0;

	conn->flag = CONNECTION_OCCUPY;

	if (ProcessRequest) res = ProcessRequest(conn->sock, &conn->data);

	if (res >= 0)
	{
		if (PostRecv(conn) < 0) CloseConnect(conn);
	}
	else if (res == XG_TIMEOUT)
	{
		time_t tm = conn->tm;

		if (PostRecv(conn) < 0) CloseConnect(conn);

		conn->tm = tm;
	}
	else if (res == XG_DETACHCONN)
	{
		conn->sock = INVALID_SOCKET;
	}
	else
	{
		CloseConnect(conn); 
	}
}
static void AcceptConnect(SOCKET sock, const char* host, int port)
{
	int res = 0;
	char address[32] = {0};
	stConnectInfo* conn = NULL;

	sock = ServerSocketAccept(sock, address);

	if (IsSocketClosed(sock)) return;
	
	Lock();
	
	if ((conn = MallocConnectNode(sock, address)) == NULL)
	{
		SocketClose(sock);
	}
#ifndef XG_LINUX
	else if (CreateIoCompletionPort((HANDLE)(sock), handle, 0, 0) == NULL)
	{
		CloseConnect(conn);
	}
#endif
	else if (ProcessConnect && (res = ProcessConnect(sock, &conn->data, host, port)) < 0)
	{
		if (res == XG_DETACHCONN)
		{
			conn->sock = INVALID_SOCKET;
		}
		else
		{
			CloseConnect(conn);
		}
	}
	else if (PostRecv(conn) < 0)
	{
		CloseConnect(conn);
	}
	
	Unlock();
}

#ifdef XG_LINUX

static int PostRecv(stConnectInfo* conn)
{
	struct epoll_event ev;

	ev.data.ptr = conn;
	ev.events = EPOLLIN | EPOLLERR | EPOLLHUP;

	if (epoll_ctl(handle, EPOLL_CTL_ADD, conn->sock, &ev) < 0) return XG_SYSERR;

	conn->flag = CONNECTION_ACTIVE;
	conn->tm = time(NULL);

	return XG_OK;
}

static void MainProcess(stWorkItem* item)
{
	int i = 0;
	int cnt = 0;
	stConnectInfo* conn = NULL;
	struct epoll_event* evs = (struct epoll_event*)malloc(XG_SOCKSVR_MAXCONNSIZE * sizeof(struct epoll_event));

	while (TRUE)
	{
		cnt = epoll_wait(handle, evs, XG_SOCKSVR_MAXCONNSIZE, XG_POLL_WAIT_TIMEOUT);

		for (i = 0; i < cnt; i++)
		{
			conn = (stConnectInfo*)(evs[i].data.ptr);

			epoll_ctl(handle, EPOLL_CTL_DEL, conn->sock, NULL);

			if (evs[i].events & EPOLLIN)
			{
				ProcessCommand(conn);
			}
			else
			{
				CloseConnect(conn);
			}
		}
	}
}

#else

static int PostRecv(stConnectInfo* conn)
{
	int res = 0;
	DWORD sz = 0;
	DWORD flag = 0;

	conn->buffer.len = 0;
	conn->buffer.buf = NULL;
	memset(&conn->overlap, 0, sizeof(conn->overlap));

	res = WSARecv(conn->sock, &conn->buffer, 1, &sz, &flag, &conn->overlap, NULL);

	if (res == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING) return XG_SYSERR;

	conn->flag = CONNECTION_ACTIVE;
	conn->tm = time(NULL);

	return XG_OK;
}
static void MainProcess(stWorkItem* item)
{
	DWORD sz = 0;
	ULONGLONG key = 0;
	stConnectInfo* conn = NULL;

	while (TRUE)
	{
		if (GetQueuedCompletionStatus(handle, &sz, (PULONG_PTR)(&key), (LPOVERLAPPED*)(&conn), XG_POLL_WAIT_TIMEOUT))
		{
			ProcessCommand(conn);			
		}
		else if (conn)
		{
			CloseConnect(conn);
		}

		conn = NULL;
	}
}

#endif

void ServerSocketLoop(const char* host, int port, int backlog, int timeout)
{
	static int inited = 0;

	if (inited == 0)
	{
		stWorkItem item;

		SocketServerInit(timeout);

#ifdef XG_LINUX
		handle = epoll_create(XG_SOCKSVR_MAXCONNSIZE);
#else
		handle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
#endif
		item.func = MainProcess;

		inited = StartThread(item, NULL);
	}

	SOCKET sock = CreateServerSocket(host, port, backlog);

	if (!IsSocketClosed(sock)) while (TRUE) AcceptConnect(sock, host, port);
}

BOOL ServerSocketAttach(SOCKET sock, const char* address)
{
	stConnectInfo* conn = NULL;

	Lock();

	if ((conn = MallocConnectNode(sock, address)) == NULL)
	{
		Unlock();

		return FALSE;
	}

	if (PostRecv(conn) < 0)
	{
		conn->sock = INVALID_SOCKET;

		Unlock();

		return FALSE;
	}

	Unlock();

	return TRUE;
}